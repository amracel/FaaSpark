package edu.hu.faa.spark;

/*
 * Uses data from a local feed redirect of the FAA airport status REST web
 * service. Parses the data and tracks each time it finds a delay.
 */


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.StorageLevels;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.Duration;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

/**
 * Read rebroadcast FAA stream via localhost. Convert JSON to AirportStatus object.
 * Create new records with IATA codes as keys and 1 value if delayed, 0 if not
 * delayed. Accumulate records.
 * Expects that web services are served locally. See included python script under
 * 'resources'.
 * Requires that Apache Spark be installed to run. 
 * See: https://spark.apache.org/docs/latest/index.html
 * Usage: spark-submit --jars json-simple-1.1.1.jar \
 * --class edu.hu.faa.spark.FaaFlightDelayStream FaaSpark-1.0-SNAPSHOT.jar 
 * @author Anne.Racel
 */
public class FaaFlightDelayStream {

    private static int SLEEP = 1000;
    private static int currentCode = 0;
    private final static String USER_AGENT = "Mozilla/5.0";
    private static SparkConf conf;
    private static final Log LOG = LogFactory
            .getLog(FaaFlightDelayStream.class);
    private static final String CHECKPOINT_DIR = "/checkpoint";


    public static void main(String[] arge) {

        LOG.info("starting code");
  
        JavaSparkContext sc = new JavaSparkContext("local[*]", "Airport Status Input");
        JavaStreamingContext jssc = new JavaStreamingContext(sc, new Duration(1000));

        JavaReceiverInputDStream<String> jsonStatus = jssc.socketTextStream(
                "localhost", 8854, StorageLevels.MEMORY_AND_DISK_SER);
        FaaFlightDelayStream stream = new FaaFlightDelayStream();
        
        // read the feed and put the data into the AirportStatus object
        JavaDStream<AirportStatus> obj = jsonStatus
                .map(new Function<String, AirportStatus>() {

                    /**
                     *
                     */
                    private static final long serialVersionUID = 1L;

                    @Override
                    public AirportStatus call(String jsonString)
                    throws Exception {
                        LOG.info("Record: " + jsonString);
                        AirportStatus airportStatus = new AirportStatus();
                        JSONParser parser = new JSONParser();
                        try {
                            JSONObject jsonObject = (JSONObject) parser
                            .parse(jsonString);
                            JSONObject weatherObject = (JSONObject) jsonObject
                            .get("weather");
                            LOG.debug("weather:" + weatherObject.toString());
                            JSONObject statusObject = (JSONObject) jsonObject
                            .get("status");
                            airportStatus.setIata(jsonObject.get("IATA")
                                    .toString());
                            airportStatus.setDelay(jsonObject.get("delay")
                                    .toString());
                            if (airportStatus.isDelay()
                            && (!statusObject.get("reason").toString()
                            .isEmpty())) {
                                airportStatus.setReason(statusObject.get(
                                                "reason").toString());
                                LOG.info("Flight delayed: " + airportStatus.getIata() + " " + airportStatus.getReason());
                            }
                            if (weatherObject.get("temp") != null) {
                                airportStatus.setTemp(weatherObject.get("temp")
                                        .toString());
                            }
                            if (weatherObject.get("wind") != null) {
                                airportStatus.setWind(weatherObject.get("wind")
                                        .toString());
                            }
                            String avgDelay = statusObject.get("avgDelay")
                            .toString();
                            if (!avgDelay.isEmpty()) {
                                airportStatus.setAvgDelay(avgDelay);
                            }

                        } catch (Exception pe) {
                            LOG.error(pe.getMessage());
                        }
                        return airportStatus;
                    }

                });
        
        jssc.checkpoint(CHECKPOINT_DIR);

        obj.print();
        // for future efforts
        
        // pull out the IATA code. if the airport shows delays,
        // feed back a '1'. if no delays, feed back a '0'
        JavaPairDStream<String, Integer> tuples = obj.mapToPair(
                new PairFunction<AirportStatus, String, Integer>() {
                    public Tuple2<String, Integer> call(AirportStatus stat) {
                        if(stat.isDelay())
                            return new Tuple2<String, Integer>(stat.getIata(), 1);
                        else 
                            return new Tuple2<String,Integer>(stat.getIata(), 0);
                    }
                }
        );
       
        // count up all the values
        JavaPairDStream<String, Integer> counts = tuples.reduceByKeyAndWindow(
                new Function2<Integer, Integer, Integer>() {
                    public Integer call(Integer i1, Integer i2) {
                        return i1 + i2;
                    }
                    
                        
                    },
                new Function2<Integer, Integer, Integer>() {
                    public Integer call(Integer i1, Integer i2) {
                        int tmp = i1 - i2;
                        if(tmp < 0)
                            return 0;
                        else
                            return tmp;
                    }
                },
                new Duration(60 * 60 * 1000),
                new Duration(1 * 1000)
        );
        counts.print();
        
        // start our streaming context and wait for it to "finish"
        jssc.start();
        LOG.info("Starting stream.");
        // Wait for 10 seconds then exit. To run forever call without a timeout
        jssc.awaitTerminationOrTimeout(SLEEP * 100);
        jssc.awaitTermination();
        LOG.info("Awaiting termination");
        // Stop the streaming context
        jssc.stop();
        LOG.info("Stopped");

    }

    
}
